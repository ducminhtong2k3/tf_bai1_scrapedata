from sqlalchemy import Column, Integer, Text, DateTime, func
from sqlalchemy.ext.declarative import declarative_base

base = declarative_base()


class Articles(base):
    __tablename__ = "data_table"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(Text)
    content = Column(Text)
    comment = Column(Text)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
