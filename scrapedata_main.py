import time
import requests
import numpy as np
import urllib3.exceptions
import validators
from bs4 import BeautifulSoup
from validators import ValidationFailure
from scrapedata_comment import scrape_comment

from db.database import session
from model.articles import Articles


root_Url = "https://vnexpress.net/giao-duc"
check = True


def is_string_an_url(url_string: str) -> bool:
    result = validators.url(url_string)
    if isinstance(result, ValidationFailure):
        return False
    return result


def scrapedata(index):

    page = requests.get(index)
    soup = BeautifulSoup(page.text, "html.parser")
    articles = soup.find_all('h3')

    for article in articles:
        # Check if page is None
        if article.find('a') is None:
            global check
            check = False
            return

        # Get links
        link = article.find('a')['href']

        # Check if link is valid
        if not is_string_an_url(link):
            continue

        # Get titles
        title = article.find('a').get_text().strip(' ').strip('\n')

        # Go to article
        sub_page = requests.get(link)

        sub_soup = BeautifulSoup(sub_page.text, "html.parser")
        sub_page_content = sub_soup.find_all('p', class_="Normal")

        # Unify paragraphs and get content
        list_paragraphs = []
        for p in np.arange(0, len(sub_page_content)):
            paragraph = sub_page_content[p].get_text()
            list_paragraphs.append(paragraph)

        content = "\n".join(list_paragraphs)

        comment = scrape_comment(link)

        # print(title, '\n', content, '\n', comment, '\n')

        # Save data to Mysql
        new_article = Articles(title=title, content=content, comment=comment)
        session.add(new_article)
        session.commit()

    session.close()


def scrapedata_page():
    n = 1
    while True:
        index = root_Url + "-p" + str(n)
        scrapedata(index)
        if check is False:
            break
        else:
            n = n + 1


def scrapedata_reload():
    try:
        scrapedata_page()
    except urllib3.exceptions.NewConnectionError:
        print("Please waiting for connection")
        time.sleep(300)


if __name__ == '__main__':
    scrapedata_reload()
