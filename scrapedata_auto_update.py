import requests
import time
from bs4 import BeautifulSoup

import scrapedata_update

root_url = "https://vnexpress.net/giao-duc"


def head_title():
    """Return head_title. """
    page = requests.get(root_url)
    soup = BeautifulSoup(page.text, "html.parser")
    title = soup.find('h3', class_="title-news").get_text()
    return title


current_title = head_title()
print("running")


def auto_update():
    """Auto update after 300s if root_Url post new article"""
    global current_title
    while True:
        new_title = head_title()
        if new_title == current_title:
            print("nothing changes")
            time.sleep(300)
        else:
            print("something changes")
            scrapedata_update.update()
            print("updated")
            current_title = head_title()
            time.sleep(300)


if __name__ == '__main__':
    auto_update()
