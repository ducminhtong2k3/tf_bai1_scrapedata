beautifulsoup4==4.11.1
numpy==1.23.2
requests==2.25.1
SQLAlchemy==1.4.41
validators==0.20.0