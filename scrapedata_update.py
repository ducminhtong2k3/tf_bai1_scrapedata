import requests
import numpy as np
from bs4 import BeautifulSoup

from db.database import session
from model.articles import Articles

root_url = "https://vnexpress.net/giao-duc"


def update():
    """Update new article. """
    page = requests.get(root_url)
    soup = BeautifulSoup(page.text, "html.parser")
    article = soup.find('h3')

    # Get new_title
    title = article.find('a').get_text().strip(' ').strip('\n')

    # Get link
    link = article.find('a')["href"]

    # Go to detail
    sub_page = requests.get(link)
    sub_soup = BeautifulSoup(sub_page.text, "html.parser")
    sub_page_content = sub_soup.find_all('p', class_="Normal")

    # Unify paragraphs and get content
    list_paragraphs = []
    for p in np.arange(0, len(sub_page_content)):
        paragraph = sub_page_content[p].get_text()
        list_paragraphs.append(paragraph)

    content = "\n".join(list_paragraphs)

    # Save data to Mysql
    new_article = Articles(title=title, content=content)
    session.add(new_article)
    session.commit()
    session.close()
