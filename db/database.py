from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from instance.database_config import user, password, host, port, database


def get_connection():
    return create_engine(
        url="mysql+pymysql://{0}:{1}@{2}:{3}/{4}".format(
            user, password, host, port, database
        )
    )


engine = get_connection()
factory = sessionmaker(bind=engine)
session = factory()
