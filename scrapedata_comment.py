import json
import requests
from bs4 import BeautifulSoup
import re


def scrape_comment(url):

    page = requests.get(url)

    soup = BeautifulSoup(page.content, 'lxml')

    info = soup.find('div', id='box_comment_vne')

    if info is None:
        return

    info = info['data-component-input']

    lines = json.loads(info)

    object_id = lines['article_id']

    object_type = lines['article_type']

    site_id = lines['site_id']

    category_id = lines['category_id']

    sign = lines['sign']

    comment_url = 'https://usi-saas.vnexpress.net/index/get?' \
                  'offset=0&limit=1000&' \
                  'frommobile=0&sort=like&is_onload=1&' \
                  f'objectid={object_id}&' \
                  f'objecttype={object_type}&' \
                  f'siteid={site_id}&' \
                  f'categoryid={category_id}&' \
                  f'sign={sign}'

    response = requests.get(comment_url)

    data = response.text

    parse_json = json.loads(data)

    comments = parse_json['data']['items']

    list_comment = []

    for comment in comments:

        list_comment.append(comment['full_name'])
        list_comment.append(comment['content'])

        parent_id = comment['parent_id']
        reply_url = f'https://usi-saas.vnexpress.net/index/getreplay?' \
                    f'siteid={site_id}&' \
                    f'objectid={object_id}&' \
                    f'objecttype={object_type}&' \
                    f'id={parent_id}&' \
                    'limit=1000&offset=0'

        sub_response = requests.get(reply_url)

        sub_data = sub_response.text

        sub_parse_json = json.loads(sub_data)

        replys = sub_parse_json['data']['items']

        for reply in replys:
            
            list_comment.append(reply['full_name'])
            list_comment.append(reply['content'])

    all_comment = "\n".join(list_comment)
    return all_comment
